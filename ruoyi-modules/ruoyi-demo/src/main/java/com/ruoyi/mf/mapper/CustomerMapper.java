package com.ruoyi.mf.mapper;

import com.mybatisflex.core.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import com.ruoyi.mf.domain.Customer;
import com.ruoyi.mf.domain.Goods;

/**
 * 客户主表Mapper接口
 *
 * @author 数据小王子
 * 2024-01-06
 */
@Mapper
public interface CustomerMapper extends BaseMapper<Customer>
{

}
